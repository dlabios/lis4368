> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Assignment 4 Requirements:

*Three parts:*

1. Edit and create more entry boxes for validation.
2. Direct the flow using servlets.
3. Compile Servlet files.

#### README.md file should include the following items:

* Screenshot of failed validation.
* Screenshot of successful validation.

#### Assignment Screenshots and Files:


*Screenshots*:

![A4 failed validation](img/failed.png)

![A4 successful validation](img/success.png)


