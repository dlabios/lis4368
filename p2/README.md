> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Project 2 Requirements:

*Three parts:*

1. Clone assignment starter files
2. Add edit and delete functionality
3. Chapter Questions(ch 16 -17)

#### README.md file should include the following items:

* Screenshot of valid user
* Screenshot of passed validation
* Screenshot of modify form
* Screenshot of displayed data
* Screenshot of delete warning
* Screenshot of database changes
* Screenshot of modified data
* Link to local host website

#### Assignment Screenshots and Files:


*Screenshots*:

| Valid User | Passed Validation |
|---|---|
| ![Valid User](img/1.PNG) | ![Passed Validation](img/2.PNG) |
| Data Display | Modify Form |
|---|---|
| ![Data Display](img/3.PNG) | ![Modify Form](img/4.PNG) |
| Modified Data | Delete Warning |
|---|---|
| ![Modified Data](img/5.PNG) | ![Delete Warning](img/6.PNG) |

