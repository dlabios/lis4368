> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Assignment 3 Requirements:

*Three parts:*

1. Create ERD using MySQL Workbench.
2. Create 10 records for each table.
3. Forward Engineer to local connection.

#### README.md file should include the following items:

* Screenshot of ERD 
* Links to a3.wmb and a3.sql

#### Assignment Screenshots and Files:


*Screenshot of SQL ERD*:

![ERD Screenshot](img/a3.png)

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")


