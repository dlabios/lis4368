-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pet_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pet_url` VARCHAR(100) NOT NULL,
  `pst_vtdSales` DECIMAL(6,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mydb`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_totalSales` DECIMAL(6,2) NOT NULL,
  `cus_url` VARCHAR(100) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mydb`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `mydb`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_saleDate` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `mydb`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `mydb`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (1, 'petsmart', '177 Water Oak Dr', 'Tallahassee', 'FL', 2025550140, 'petsmart@gmail.com', 'petsmart.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (2, 'petco', '328 Lemon St', 'Tallahassee', 'FL', 7275551175, 'petco@gmail.com', 'petco.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (3, 'humane society', '919 MLK Blvd', 'Tallahassee', 'FL', 2025558110, 'hsociety@gmail.com', 'hsociety.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (4, 'tally cat cafe', '2218 N Monroe St', 'Tallahassee', 'FL', 2025550109, 'tccafe@gmail.com', 'tccafe.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (5, 'tall cat cafe', '678 W Tenessee St', 'Tallahassee', 'FL', 8505556111, 'tallcatcafe@gmail.com', 'tallcatcafe.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (6, 'green pets', '9020 N Monroe St', 'Tallahassee', 'FL', 8505550132, 'greenpets@gmail.com', 'greenpets.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (7, 'pets lanes', '210 W Tennessee St', 'Tallahassee', 'FL', 7275556278, 'petslanes@gmail.com', 'petslanes.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (8, 'cat town', '4166 N Monroe St', 'Tallahassee', 'FL', 8135552927, 'cattown@gmail.com', 'cattown.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (9, 'catty', '1090 N Monroe St', 'Tallahassee', 'FL', 8505559019, 'catty@gmail.com', 'catty.com', 1, NULL);
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pet_city`, `pst_state`, `pst_phone`, `pst_email`, `pet_url`, `pst_vtdSales`, `pst_notes`) VALUES (10, 'the pet store', '2552 N Monroe St', 'Tallahassee', 'FL', 2025552782, 'thepetstore@gmail.com', 'thepetstore', 1, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (1, 'Benjamin', 'Smith', '109 Lilac Ln', 'Tallahassee', 'FL', 32304, 8135550987, 'bsmith@gmail.com', 80.00, 1, 'bsmith.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (2, 'Evelyn', 'Johnson', '72 10th St', 'Tallahassee', 'FL', 32304, 7275558363, 'ejohnson@gmail.com', 80.00, 1, 'ejohnson.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (3, 'Noah', 'Williams', '578 Lexington Dr', 'Tallahassee', 'FL', 32304, 8505558375, 'nwiliiams@gmail.com', 80.00, 1, 'nwilliams.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (4, 'Mia', 'Jones', '1030 5th Ave', 'Tallahassee', 'FL', 32304, 8505550074, 'mjones@gmail.com', 100.00, 1, 'mjones.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (5, 'James', 'Brown', '813 Henry St', 'Tallahassee', 'FL', 32304, 8505551224, 'jbrown@gmail.com', 150.00, 2, 'jbrown.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (6, 'Charlotte', 'Davis', '2019 Walnut Ave', 'Tallahassee', 'FL', 32304, 2125558392, 'cdavis@gmail.com', 80.00, 1, 'cdavis.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (7, 'Elijah', 'Miller', '573 Mill St', 'Tallahassee', 'FL', 32304, 8135558985, 'emiller@gmail.com', 80.00, 2, 'emiller.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (8, 'Abigail', 'Wilson', '922 Jefferson Ct', 'Tallahassee', 'FL', 32304, 8505557765, 'awilson@gmail.com', 80.00, 1, 'awilson.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (9, 'Jacob', 'Moore', '878 Tennessee St', 'Tallahassee', 'FL', 32304, 8505553759, 'jmoore@gmail.com', 100.00, 1, 'jmoore.com', NULL);
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_totalSales`, `cus_url`, `cus_notes`) VALUES (10, 'Scott', 'Taylor', '6728 Harrison Rd', 'Tallahassee', 'FL', 32304, 7275551573, 'staylor@gmail.com', 100.00, 1, 'staylor.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 1, 'cat', 'f', 20.00, 80.00, 1, 'calico', '2019-01-21', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 2, 'dog', 'm', 20.00, 80.00, 2, 'white', '2019-01-30', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 3, 'cat', 'f', 20.00, 80.00, 2, 'black', '2019-01-01', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 4, 'cat', 'm', 50.00, 100.00, 1, 'black', '2018-02-28', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 5, 'cat', 'm', 75.00, 150.00, 3, 'tan', '2018-03-3', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 6, 'dog', 'm', 20.00, 80.00, 1, 'gold', '2019-01-02', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 7, 'cat', 'f', 20.00, 80.00, 7, 'white', '2019-01-18', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 8, 'cat', 'm', 20.00, 80.00, 2, 'calico', '2018-10-22', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 9, 'cat', 'm', 50.00, 100.00, 1, 'grey', '2019-02-03', 'y', 'y', NULL);
INSERT INTO `mydb`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_saleDate`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 10, 'dog', 'f', 50.00, 100.00, 3, 'brown', '2019-01-05', 'y', 'y', NULL);

COMMIT;

