> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Assignment 1 Requirements:

*Three parts:*

1. Distributed Version Control with Git and Bitbucket installation
2. Java/JSP/Servelet development installation
3. Chapter questions (Ch. 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot if running http://localhost:9999
* Description of git commands
* Bitbucket repo links a) this assignment and b)the completed tutorial above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates or reinitializes empty repository 
2. git status - provides a status of which files have changes that are set for the next commit.
3. git add - add new or modified files to the index.
4. git commit - Records Changes to repository
5. git push - uploads remote refs to corresponding  upstream branch.
6. git pull - retrieves from a remote repo to another local branch.
7. git clone - Copies a repository into a new directory.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost:9999*:

![AMPPS Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
