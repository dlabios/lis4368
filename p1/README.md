> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Project 1 Requirements:

*Three parts:*

1. Learn JQuery validation
2. Learn regexp statements
3. Test web application

#### README.md file should include the following items:

*  Screenshot of failed validation
*  Screenshot of successful validation
*  Screenshot of 3 slide from carousel

#### Assignment Screenshots and Files:


*Screenshots:

![Failed validation screenshot](global/img/failed.PNG)

![Successful validation screenshot](global/img/success.PNG)

![Carousel slide 1 screenshots](global/img/c1.PNG)

![Carousel slide 2 screenshots](global/img/c2.PNG)

![Carousel slide 3 screenshots](global/img/c3.PNG)
