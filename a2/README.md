> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Develoopment

## Derek Labios

### Assignment 2 Requirements:

*Three parts:*

1. Write and compile servlet files
2. Deploy and test servlet
3. Chapter questions (Ch. 5-6)

#### README.md file should include the following items:

*Screenshot of the following: *

1. http://localhost:9999/hello (displays directory, needs index.html)
2. http://localhost:9999/hello/HelloHome.html
3. http://localhost:9999/hello/sayhello
4. http://localhost:9999/hello/querybook.html
5. http://localhost:9999/hello/sayhi

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost:9999/hello/query?author=Tan+Ah+Teck&author=Mohammad+Ali&author=Kumar:

![Querybook Results screenshot](img/results.png)



