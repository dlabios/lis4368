> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications

## Derek Labios

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Apache Tomcat
    - Provide screenshots of installations
    - Create Bitbucket rep
    - Complete Bitbucket tutorials
    - Describe git commands

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Develop and deploy a Webapp
    - Write a "Hello-World" Java Servlet
    - Write a Database Servlet
    - Deployed Servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Develop ERD for relationshsip.
    - Write SQL statements for table
    - Describe business rules for relationships
    - Provide screenshot of ERD

3. [P1 README.md](p1/README.md "My P1 README.md file")
    - Developed JQuery validation statements
    - Learn regexp language and incorporate it into we application
    - Edit carousel links and images

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone assignment student files
    - Modify files to add serverside validation
    - Compile class and servlet files

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create server side validation
    - Connect web app to database
    - Compile servlet files

3. [P2 README.md](p2/README.md "My P2 README.md file")
    - Clone student starter files
    - Connect database to website
    - Add edit and delete functionality
    - Compile class and servlet files