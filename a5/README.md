> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Derek Labios

### Assignment 5 Requirements:

*Three parts:*

1. Create server side validation
2. Connect web app to database
3. Compile servlet files/

#### README.md file should include the following items:

* Screenshot of valid entries.
* Screenshot of successful entries.
* Screenshot of data tables.

#### Assignment Screenshots and Files:


*Screenshots*:


| Screenshot valid entries | Screenshot passed inputs |
|---|---|
| ![A5 valid entries](img/valid.png) | ![A5 passed inputs](img/passed.png) |
Screenshot Data table
![A5 mysql table](img/data.png)


